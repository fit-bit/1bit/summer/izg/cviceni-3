IZG Cvičení 3
-------------
Body: 3/3

#### Ovládání aplikace
##### Stisknutí levého tlačítka myši
Přidá nový bod k polygonu

##### Stisknutí pravého tlačítka myši
Vyplní oblast (Pinedův algoritmus pro vyplňování polygonů)

##### S
Uloží aktuální obraz

##### C
Vymazal framebuffer

##### 0-9
Spustí testy od 0 do 9 (Pinedův algoritmus pro trojúhelníky 0-3, Pinedův algoritmus pro mnohoúhelníky 4-9)

##### T
Nastaví vyplnění na Pinedův algoritmus pro trojúhelníky

##### P
Nastaví vyplnění na Pinedův algoritmus pro mnohoúhelníky
 
##### R
Nastaví barvu vyplnění na červenou
 
##### G
Nastaví barvu vyplnění na zelenou

##### B
Nastaví barvu vyplnění na modrou

##### W
Nastaví barvu vyplnění na bílém

##### K
Nastaví barvu vyplnění na černou

##### CTRL + [RGBWK]
Nastaví druhou barvu pro barvu hran polygonů